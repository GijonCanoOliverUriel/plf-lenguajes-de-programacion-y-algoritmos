# Mapa conceptual 1
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .verde {
    BackgroundColor lightgreen
  }
  .rosa {
    BackgroundColor #FFBBCC
  }
  .naranja {
    BackgroundColor #FF5733
  }
}
</style>
* Programando ordenadores en \nlos 80 y ahora. ¿Qué ha cambiado? (2016) node <<naranja>>
	* Sistema Antiguo
		* Consiste en node <<rosa>>
			* Sistemas que no tuvieron continuidad
				* Desarrollados en
					* Los años 80
		* Lenguaje de progrmacion node <<rosa>>
			* Para programar se necesitaba
				* Conocer a detalle la maquina
			* Ensamblador
				* Se tiene el control total del programa
					* Se le dice a la maquina que hacer
	* Sistema actual
		* Lenguaje de progrmacion node <<rosa>>
			* Lenguajes de alto nivel
				* Trabaja con ayuda de compiladores o interpretes
				 * Por lo que el programador ya no necesita conocer la maquina
			* Consecuencia
				* El software ocupa muchos recursos
					* Hace el equipo lento
						* Se necesitan librerías masivas
@endmindmap
```
# Mapa conceptual 2
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .verde {
    BackgroundColor lightgreen
  }
  .rosa {
    BackgroundColor #FFBBCC
  }
  .naranja {
    BackgroundColor #FF5733
  }
}
</style>
* Hª de los algoritmos y de los lenguajes de programación (2010) node <<rosa>>
	* Lenguajes de Programación node <<verde>>
		* Paradigmas inciales
			* Paradigma Imperativo(1959)
			* Paradigma Funcional(1960)
			* Lenguaje orientado a objetos(1968)
			* Paradigma Lógico(1971)
	* Algoritmos node <<verde>>
		*_ Se originan
			* Mucho antes de las computadoras
				* Su primera relacion con la informática
					* Las calculadoras
		*_ Consiste en
			* Tipos de coste
				* Razonables
				* No Razonables
			* Es un procedimiento para resolver un problema
				*_ Sus limites
					* En los años 30, problemas matematicos complejos
					* problemas indecibles
@endmindmap
```
# Mapa conceptual 3
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .verde {
    BackgroundColor lightgreen
  }
  .rosa {
    BackgroundColor #FFBBCC
  }
  .naranja {
    BackgroundColor #FF5733
  }
}
</style>
* Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. \nLa evolución de los lenguajes y paradigmas de programación (2005). node <<verde>>
	* Mas comunes node <<naranja>>
		* Programación Estructurada
			* Lenguajes
				* Basic
				* pascal
				* java
				* c
			* Ofrece 
				* Una concepción mas modular al programar
		* Paradigma funcional
			*_ Consiste en
				* Apoyarse de las matemáticas
					* Usa la herramienta 
						* Recursividad
		* Paradigma Lógico
			*_ Se basa en
				* Expresiones logicas (Cierto o falso)
					* Consiste en
						* Modelizar un problema
	* En tendencias node <<naranja>>
		* Programación concurrente
			*_ Pretende
				* Dar solución a multitud de usuarios que quieren accerder
		* Programación distribuida
			*_ Surge por
				* La comunicación entre múltiples ordenadores
		* Programación orientada a objetos
			*_ Ofrece
				* Un algoritmo como un dialogo entre los objetos
		* Programación basada en componentes 
			*_ Consiste en
				* Reutilizar código
@endmindmap
```


